#[derive(Debug)]
struct ListNode {
    value: i32,
    next: Option<Box<ListNode>>,
}

fn push_front(head: &mut Option<Box<ListNode>>, value: i32) {
    let old_head = head.take();
    *head = Some(Box::new(ListNode {
        value,
        next: old_head,
    }));
}

fn push_back(head: &mut Option<Box<ListNode>>, value: i32) {
    let mut r = head.as_mut();
    while r.as_ref().and_then(|node| node.next.as_ref()).is_some() {
        r = r.and_then(|node| node.next.as_mut());
    }
    match r {
        None => {
            *head = Some(Box::new(ListNode { value, next: None }));
        }
        Some(node) => node.next = Some(Box::new(ListNode { value, next: None })),
    }
}

fn add_two_numbers(
    mut l1: Option<Box<ListNode>>,
    mut l2: Option<Box<ListNode>>,
) -> Option<Box<ListNode>> {
    let mut res = None;
    let mut carry = false;

    while l1.is_some() || l2.is_some() {
        let mut sum = l1.as_ref().map(|node| node.value).unwrap_or_default()
            + l2.as_ref().map(|node| node.value).unwrap_or_default()
            + if carry { 1 } else { 0 };
        carry = sum >= 10;
        sum %= 10;

        push_back(&mut res, sum);

        l1 = l1.and_then(|node| node.next);
        l2 = l2.and_then(|node| node.next);
    }
    if carry {
        push_back(&mut res, 1);
    }
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut l1 = None;
        push_front(&mut l1, 3);
        push_front(&mut l1, 4);
        push_front(&mut l1, 2);

        let mut l2 = None;
        push_front(&mut l2, 4);
        push_front(&mut l2, 6);
        push_front(&mut l2, 5);

        dbg!(add_two_numbers(l1, l2)); // must be 7 -> 0 -> 8
    }

    #[test]
    fn test_push_back_front() {
        let mut list = None;
        dbg!(&list);
        push_back(&mut list, 0);
        dbg!(&list); // must be Some(0, None)

        let mut list = None;
        push_front(&mut list, 11);
        dbg!(list); // must be Some(11, None)

        let mut list = None;
        push_back(&mut list, 11);
        dbg!(list); // must be Some(11, None)

        let mut list = None;
        push_back(&mut list, 1);
        push_front(&mut list, 2);
        push_back(&mut list, 3);
        dbg!(list); // must be 2 -> 1 -> 3
    }
}
