fn clear(array: &mut u128) {
    *array = 0
}

fn set(array: &mut u128, c: u8) {
    *array |= 1u128 << c as u128;
}

fn get(array: u128, c: u8) -> bool {
    (array & (1u128 << c as u128)) > 0
}

fn length_of_longest_substring(s: String) -> i32 {
    let len = s.len();
    let s = s.as_bytes();
    let mut i = 0;
    let mut array: u128 = 0;
    let mut max = 0;
    let mut curr = 0;
    while i < len {
        let c = s[i];
        if get(array, c) {
            i -= curr - 1;
            curr = 0;
            clear(&mut array);
        } else {
            curr += 1;
            i += 1;
            set(&mut array, c);
        }
        if curr > max {
            max = curr;
        }
    }
    max as _
}

#[cfg(test)]
mod tests {
    use super::*;
    fn helper(str: &str, res: i32) {
        assert_eq!(length_of_longest_substring(String::from(str)), res);
    }

    #[test]
    fn test() {
        helper("abcabcabc", 3);
        helper("bbbbb", 1);
        helper("pwwkew", 3);
        helper("", 0);
        helper("x", 1);
        helper("dvdf", 3);
    }
}
