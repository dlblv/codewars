fn is_palindrome(mut x: i32) -> bool {
    if x < 0 {
        return false;
    }

    let digits = {
        let mut digits = vec![];
        while x != 0 {
            digits.push(x % 10);
            x /= 10;
        }
        digits
    };
    digits.iter().zip(digits.iter().rev()).all(|(x, y)| x == y)
}

#[cfg(test)]
#[test]
fn test() {
    assert!(is_palindrome(1));
    assert!(!is_palindrome(10));
    assert!(!is_palindrome(-123));
    assert!(is_palindrome(1));
    assert!(is_palindrome(12321));
}
