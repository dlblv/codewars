use std::collections::hash_map::HashMap;

fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
    let mut map = HashMap::<i32, usize>::with_capacity(nums.len());
    for (idx, num) in nums.into_iter().enumerate() {
        match map.get(&(target - num)) {
            Some(idx2) => return vec![idx as i32, *idx2 as i32],
            None => {
                map.insert(num, idx);
            }
        }
    }
    vec![]
}

#[cfg(test)]
mod tests {
    fn helper(nums: impl AsRef<[i32]>, target: i32) {
        let solution = super::two_sum(nums.as_ref().into(), target);
        let ai = solution[0];
        let bi = solution[1];
        assert_ne!(ai, bi);

        let a = nums.as_ref()[ai as usize];
        let b = nums.as_ref()[bi as usize];

        assert_eq!(a + b, target);
    }

    #[test]
    fn two_sum_test() {
        helper([1, 2, 3, 4], 5);
        helper([2, 3, 4, 5], 6);
    }
}
