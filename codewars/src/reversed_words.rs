// https://www.codewars.com/kata/51c8991dee245d7ddf00000e/train/rust

fn reverse_words(input: &str) -> String {
    input.split(' ').rev().collect::<Vec<_>>().join(" ")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn main() {
        assert_eq!("a b c d", reverse_words("d c b a"));
    }
}
