/// https://www.codewars.com/kata/5511b2f550906349a70004e1/train/rust

fn last_digit(str1: &str, str2: &str) -> i32 {
    if str2 == "0" {
        return 1;
    }
    let base = str1.chars().last().unwrap().to_digit(10).unwrap() as i32;
    let exponent = str2[str2.len().saturating_sub(2)..].parse::<i32>().unwrap() % 4;
    base.pow(exponent as u32 + 4) % 10
}

#[cfg(test)]
#[test]
fn returns_expected() {
    assert_eq!(last_digit("4", "1"), 4);
    assert_eq!(last_digit("4", "2"), 6);
    assert_eq!(last_digit("9", "7"), 9);
    assert_eq!(last_digit("10", "10000000000"), 0);
    assert_eq!(last_digit("1606938044258990275541962092341162602522202993782792835301376","2037035976334486086268445688409378161051468393665936250636140449354381299763336706183397376"), 6);
    assert_eq!(
        last_digit(
            "3715290469715693021198967285016729344580685479654510946723",
            "68819615221552997273737174557165657483427362207517952651"
        ),
        7
    );
}
