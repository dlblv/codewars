fn spin_words(words: &str) -> String {
    words
        .split_ascii_whitespace()
        .map(|word| {
            if word.len() >= 5 {
                word.chars().rev().collect()
            } else {
                String::from(word)
            }
        })
        .collect::<Vec<_>>()
        .join(" ")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn main() {
        println!("{}", spin_words("a bb abcde recognition box rock leisure"));
    }
}
