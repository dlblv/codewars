// https://www.codewars.com/kata/56f78a42f749ba513b00037f/train/rust

fn rolldice_sum_prob(sum: i32, dice_amount: i32) -> f64 {
    let r = 6i32.pow(dice_amount as _);
    let mut count = 0.;
    for i in 0..r {
        let mut dices = Vec::<i32>::with_capacity(dice_amount as _);
        let mut x = i;
        for _j in 0..dice_amount {
            dices.push(x % 6 + 1);
            x /= 6;
        }
        if dices.iter().sum::<i32>() == sum {
            count += 1.;
        }
    }
    count / r as f64
}

#[cfg(test)]
mod tests {
    use super::*;
    use float_eq::float_eq;

    fn assert_float_equals(actual: f64, expected: f64, merr: f64) {
        let res =
            float_eq!(actual, expected, abs <= merr) || float_eq!(actual, expected, rmax <= merr);
        assert!(
            res,
            "Expected value must be near: {:e} but was: {:e}",
            expected, actual
        );
    }

    #[test]
    fn main() {
        assert_float_equals(rolldice_sum_prob(11, 2), 0.055555555555, 1e-10);
        assert_float_equals(rolldice_sum_prob(8, 2), 0.13888888889, 1e-10);
        assert_float_equals(rolldice_sum_prob(8, 3), 0.0972222222222, 1e-10);
    }
}
