use std::mem;

use num::{BigUint, One, Zero};

fn all_fibonacci_numbers() -> impl Iterator<Item = BigUint> {
    FibonacciGun::default()
}

struct FibonacciGun {
    prev: BigUint,
    prev_prev: BigUint,
}

impl Default for FibonacciGun {
    fn default() -> Self {
        Self {
            prev: BigUint::zero(),
            prev_prev: BigUint::one(),
        }
    }
}

impl Iterator for FibonacciGun {
    type Item = BigUint;
    fn next(&mut self) -> Option<Self::Item> {
        let next = &self.prev_prev + &self.prev;
        mem::replace(&mut self.prev_prev, mem::take(&mut self.prev));
        self.prev.clone_from(&next);
        Some(next)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use num::BigUint;

    fn test_segment(start: u32, numbers: [u128; 10]) {
        let mut fib_iterator = all_fibonacci_numbers();
        for _ in 0..start {
            fib_iterator.next();
        }
        for i in numbers {
            let ans = fib_iterator.next();
            let expected = Some(BigUint::from(i));
            assert_eq!(
                expected, ans,
                "\nYour result (left) \ndid not match the expected output (right)"
            );
        }
    }

    #[test]
    fn tests_0_10() {
        test_segment(0, [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]);
    }

    #[test]
    fn tests_10_20() {
        test_segment(10, [89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765]);
    }

    #[test]
    fn tests_100_110() {
        test_segment(
            100,
            [
                573147844013817084101,
                927372692193078999176,
                1500520536206896083277,
                2427893228399975082453,
                3928413764606871165730,
                6356306993006846248183,
                10284720757613717413913,
                16641027750620563662096,
                26925748508234281076009,
                43566776258854844738105,
            ],
        );
    }
}
