#![allow(unused)]

mod anagrams;
mod bleatrix_trotter;
mod dice_probability;
mod factorial_zeros;
mod fibonacci_streaming;
mod last_digit;
mod mumbling;
mod postfix_calc;
mod prime_streaming_pg13;
mod reversed_words;
mod sequence_convergence;
mod smallest;
mod snail;
mod spin_words;
mod tribonacci_sequence;
mod your_order;
