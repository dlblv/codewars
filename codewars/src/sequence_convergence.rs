struct Digitizer<T> {
    pub value: T,
    pub base: T,
}

impl<T> Iterator for Digitizer<T>
where
    T: std::cmp::PartialOrd
        + std::ops::DivAssign
        + std::convert::From<u8>
        + std::ops::Rem<Output = T>
        + Copy,
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        if self.value > 0u8.into() {
            let res = self.value % self.base;
            self.value /= self.base;
            Some(res)
        } else {
            None
        }
    }
}

struct Series(u32);

impl Series {
    pub fn value(&self) -> u32 {
        self.0
    }
}

impl Iterator for Series {
    type Item = u32;
    fn next(&mut self) -> Option<Self::Item> {
        let res = self.0;
        self.0 = if self.0 < 10 {
            self.0 * 2
        } else {
            let a = Digitizer {
                value: self.0 as _,
                base: 10,
            };
            let a: u32 = a.filter(|&x| x != 0).product();
            self.0 + a
        };
        Some(res)
    }
}

fn convergence(n: u32) -> usize {
    let mut base = Series(1);
    let mut test = Series(n);
    let mut res = 0;
    while base.value() != test.value() {
        if base.value() > test.value() {
            test.next().unwrap();
            res += 1;
        } else {
            base.next().unwrap();
        }
    }
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_digitizer() {
        let a = Digitizer {
            value: 123456,
            base: 10,
        };
        assert_eq!(vec![6, 5, 4, 3, 2, 1], a.collect::<Vec<_>>());
    }

    #[test]
    fn test_series() {
        let s = Series(1);
        assert_eq!(
            vec![1, 2, 4, 8, 16, 22, 26, 38, 62, 74, 102, 104, 108, 116, 122],
            s.take(15).collect::<Vec<_>>()
        );
        let s = Series(3);
        assert_eq!(
            vec![3, 6, 12, 14, 18, 26, 38, 62, 74, 102, 104, 108, 116, 122],
            s.take(14).collect::<Vec<_>>()
        );
        let s = Series(15);
        assert_eq!(
            vec![15, 20, 22, 26, 38, 62, 74, 102, 104, 108, 116, 122],
            s.take(12).collect::<Vec<_>>()
        );
    }

    #[test]
    fn basic_tests() {
        assert_eq!(convergence(3), 5);
        assert_eq!(convergence(5), 6);
        assert_eq!(convergence(10), 5);
        assert_eq!(convergence(15), 2);
        assert_eq!(convergence(500), 29);
        assert_eq!(convergence(5000), 283);
    }
}
