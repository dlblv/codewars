// https://www.codewars.com/kata/55c45be3b2079eccff00010f/train/rust
fn order(sentence: &str) -> String {
    let mut words = sentence.split_ascii_whitespace().collect::<Vec<_>>();
    words.sort_by_key(|s| -> u32 {
        for ch in s.chars() {
            if ch.is_ascii_digit() {
                return ch.to_digit(10).unwrap();
            }
        }
        unreachable!()
    });
    words.join(" ")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn main() {
        assert_eq!(order("is2 Thi1s T4est 3a"), "Thi1s is2 3a T4est");
        assert_eq!(order("a2 b1"), "b1 a2");
    }
}
