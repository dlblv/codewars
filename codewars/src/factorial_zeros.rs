fn zeros(n: u64) -> u64 {
    let n = n as f64;
    let kmax = n.log(5.).floor() as u64;
    let mut z = 0.;
    for i in 1..=kmax {
        z += (n / (5.0_f64.powf(i as f64))).floor();
    }
    z as u64
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn main() {
        assert_eq!(zeros(0), 0);
        assert_eq!(zeros(6), 1);
        assert_eq!(zeros(14), 2);
        assert_eq!(zeros(30), 7);
        assert_eq!(zeros(1000), 249);
        assert_eq!(zeros(100000), 24999);
        assert_eq!(zeros(1000000000), 249999998);
    }
}
