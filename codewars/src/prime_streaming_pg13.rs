//! https://www.codewars.com/kata/5519a584a73e70fa570005f5/train/rust

use std::collections::HashSet;

fn stream() -> impl Iterator<Item = u32> {
    fn is_prime(n: u32) -> bool {
        let sqrt = (n as f64).sqrt().ceil() as u32;
        for i in 2..=sqrt {
            if n % i == 0 {
                return false;
            }
        }
        true
    }

    struct Primer {
        last_prime: u32,
    }

    impl Iterator for Primer {
        type Item = u32;
        fn next(&mut self) -> Option<Self::Item> {
            for n in self.last_prime + 1.. {
                if is_prime(n) {
                    self.last_prime = n;
                    return Some(n);
                }
            }
            unreachable!()
        }
    }

    Primer { last_prime: 1 }
}

#[test]
fn bench() {
    use std::time::Instant;
    let begin = Instant::now();
    stream().take(1_000_000).all(|_| true);
    println!("Elapsed: {:?}", Instant::now().duration_since(begin));
}

#[cfg(test)]
mod tests {
    #[test]
    fn unsolved() {
        panic!("THIS TASK IS NOT FULLY SOLVED!!!")
    }

    use super::*;

    fn test_segment(start: u32, numbers: [u32; 10]) {
        let mut prime_iterator = stream();
        for _ in 0..start {
            prime_iterator.next();
        }
        for i in numbers {
            assert_eq!(
                Some(i),
                prime_iterator.next(),
                "\nYour result (left) did not match the expected output (right)"
            );
        }
    }

    #[test]
    fn test() {
        println!("testing segment from 0");
        test_segment(0, [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]);

        println!("testing segment from 10");
        test_segment(10, [31, 37, 41, 43, 47, 53, 59, 61, 67, 71]);

        println!("testing segment from 100");
        test_segment(100, [547, 557, 563, 569, 571, 577, 587, 593, 599, 601]);

        println!("testing segment from 1,000");
        test_segment(
            1_000,
            [7927, 7933, 7937, 7949, 7951, 7963, 7993, 8009, 8011, 8017],
        );
    }
}
