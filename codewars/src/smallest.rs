// https://www.codewars.com/kata/573992c724fc289553000e95/train/rust

use itertools::iproduct;

fn smallest(n: i64) -> (i64, usize, usize) {
    let num = n.to_string();
    let len = num.len();
    iproduct!(0..len, 0..len)
        .zip(std::iter::repeat(num))
        .map(|((i, j), mut num)| {
            let digit = num.chars().nth(i).unwrap();
            num.remove(i);
            num.insert(j, digit);
            (num.parse::<i64>().unwrap(), i, j)
        })
        .min()
        .unwrap()
}

#[cfg(test)]
#[test]
fn test() {
    assert_eq!(smallest(1321521981065949846), (132152198165949846, 10, 0));
}
