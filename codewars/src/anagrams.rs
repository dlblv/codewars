fn is_anagram(x: &str, y: &str) -> bool {
    let mut x = x.chars().collect::<Vec<_>>();
    let mut y = y.chars().collect::<Vec<_>>();
    x.sort_unstable();
    y.sort_unstable();
    x == y
}

fn anagrams(word: &str, words: &[String]) -> Vec<String> {
    let mut res = Vec::new();
    for w in words {
        if is_anagram(word, w) {
            res.push(w.clone());
        }
    }
    res
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn anagrams() {
        assert!(is_anagram("abba", "bbaa"));
        assert!(is_anagram("aabb", "abab"));
        assert!(is_anagram("abba", "baba"));
        assert!(is_anagram("abba", "aabb"));
        assert!(!is_anagram("abba", "abbb"));
    }
}
