fn postfix_evaluator(expr: &str) -> i64 {
    let mut operands = Vec::<i64>::with_capacity(100);
    for token in expr.split_ascii_whitespace() {
        match token {
            "*" => {
                let b = operands.pop().unwrap();
                let a = operands.pop().unwrap();
                operands.push(a * b);
            }
            "+" => {
                let b = operands.pop().unwrap();
                let a = operands.pop().unwrap();
                operands.push(a + b);
            }
            "-" => {
                let b = operands.pop().unwrap();
                let a = operands.pop().unwrap();
                operands.push(a - b);
            }
            "/" => {
                let b = operands.pop().unwrap();
                let a = operands.pop().unwrap();
                operands.push(a / b);
            }
            _ => {
                operands.push(token.parse().unwrap());
            }
        }
    }
    operands.pop().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sub() {
        assert_eq!(postfix_evaluator("2 3 -"), -1);
    }
    #[test]
    fn basic() {
        // Simple addition
        assert_eq!(postfix_evaluator("2 3 +"), 5);

        // Addition with negative numbers
        assert_eq!(postfix_evaluator("2 -3 +"), -1);

        // Constant numbers
        assert_eq!(postfix_evaluator("1"), 1);
        assert_eq!(postfix_evaluator("-1"), -1);

        // Complex expressions
        assert_eq!(postfix_evaluator("2 3 9 4 / + *"), 10);
        assert_eq!(postfix_evaluator("3 4 9 / *"), 0);
        assert_eq!(postfix_evaluator("4 8 + 6 5 - * 3 2 - 2 2 + * /"), 3);

        // Multi-digit
        assert_eq!(postfix_evaluator("21 21 +"), 42);
    }
}
