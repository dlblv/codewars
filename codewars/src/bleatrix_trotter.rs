// https://www.codewars.com/kata/59245b3c794d54b06600002a

fn trotter(n: i32) -> i32 {
    if n == 0 {
        return -1;
    }
    let mut numbers = 0;
    for i in 1..=i32::MAX / n {
        for c in (n * i).to_string().chars() {
            numbers |= 1 << (c.to_digit(10).unwrap());
        }
        if numbers == 0b1111111111 {
            return n * i;
        }
    }
    -1
}
