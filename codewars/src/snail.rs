// https://www.codewars.com/kata/521c2db8ddc89b9b7a0000c1/train/rust
fn snail(matrix: &[Vec<i32>]) -> Vec<i32> {
    if matrix.is_empty() {
        return Vec::new();
    }
    let n = matrix[0].len();
    if n == 0 {
        return Vec::new();
    }
    let mut res = Vec::with_capacity(n * n);
    let mut x;
    let mut y;
    let mut step = n - 1;
    while step > 0 {
        x = (n - step - 1) / 2;
        y = (n - step - 1) / 2;
        for i in 0..4 {
            for _ in 0..step {
                res.push(matrix[x][y]);
                match i {
                    0 => {
                        y += 1;
                    }
                    1 => {
                        x += 1;
                    }
                    2 => {
                        y -= 1;
                    }
                    3 => {
                        x -= 1;
                    }
                    _ => unreachable!(),
                }
            }
        }
        if step == 1 {
            break;
        } else {
            step -= 2;
        }
    }
    if step == 0 {
        res.push(matrix[(n - 1) / 2][(n - 1) / 2]);
    }
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test3x3() {
        let square = &[vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
        let expected = vec![1, 2, 3, 6, 9, 8, 7, 4, 5];
        assert_eq!(snail(square), expected);
    }

    #[test]
    fn test4x4() {
        let square = &[
            vec![1, 2, 3, 4],
            vec![5, 6, 7, 8],
            vec![9, 10, 11, 12],
            vec![13, 14, 15, 16],
        ];
        let expected = vec![1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10];
        assert_eq!(snail(square), expected);
    }

    #[test]
    fn sample_test2() {
        let square = &[vec![1, 2, 3], vec![8, 9, 4], vec![7, 6, 5]];
        let expected = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];
        assert_eq!(snail(square), expected);
    }

    #[test]
    fn sample_test3() {
        let square: &[Vec<i32>; 1] = &[Vec::new()];
        let expected = Vec::new();
        assert_eq!(snail(square), expected, "Failed with empty input");
    }

    #[test]
    fn sample_test4() {
        let square = &[vec![1]];
        let expected = vec![1];
        assert_eq!(snail(square), expected);
    }
}
